cmake_minimum_required(VERSION 3.20)
project(serial_protocol)

set(CMAKE_CXX_STANDARD 17)

add_library(serial_protocol INTERFACE mserialProtocol.h)
